
const mongoose = require('mongoose')
const dotenv = require('dotenv')
dotenv.config({path : './config.env'})
const app =require('./app');

const DB = process.env.DATABASE.replace(
    'PASSWORD'
    ,process.env.DATABASE_PASSWORD,
    )
// console.log(process.env.DATABASE_PASSWORD)

const DATABASE_LOCAL = process.env.DATABASE_LOCAL

mongoose.connect(DB).then((con) =>{
    console.log(con.connections)
    console.log('DB connection successful')

}).catch(error =>console.log(error));


/* startign the server on port*/
const port = 4001;
app.listen(port, ()=>{
    console.log(`app running on the port ${port} .. `)
})

